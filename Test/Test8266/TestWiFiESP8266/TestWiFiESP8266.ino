
#include <ESP8266WiFi.h>

const char* ssid = "Home&Life SuperWiFi-B8E1";
const char* password = "JBFX4ECMBK8T7KFJ";

int Led = 2; //GPIO2---D4 of Nodemcu
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  delay(10);
  pinMode(Led, OUTPUT);
  digitalWrite(Led, LOW);
  //Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

    // Print the IP address on serial monitor
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");    //URL IP to be typed in mobile/desktop browser
  Serial.print(WiFi.localIP());
  Serial.println("/");

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(Led, !digitalRead(Led));
  delay(500);
}
