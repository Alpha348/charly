#include <AFMotor.h>
#include <Wire.h>


#define Addr 0x08
#define Led 2

AF_DCMotor MSx(4);
AF_DCMotor MDx(3);

byte Connect = LOW;
char Cmd = '0';
byte sData = LOW;
String txtMsg = "";
unsigned int lStr = 0;
byte L = 0;
byte Modo = 0;
byte Vmin = 0;
byte Vmax = 0;
byte Vnow = 0;
byte Val = 0;

void setup(){
  Wire.begin(Addr);
  Serial.begin(9600);
  Wire.onReceive(ReceiveCMD);
  Wire.onRequest(SData);
  pinMode(Led, OUTPUT);
  digitalWrite(Led, HIGH);
  delay(500);
  digitalWrite(Led, LOW);
  MSx.run(RELEASE);
  MDx.run(RELEASE);
  Serial.println("System ready");

}

void loop(){
  switch (Cmd) {
    case '0':
      break;
    case 'v'://Set della Vmin
      if (Connect){
        Modo=1;
        Msg("D");
        Cmd = '0';
      }else{
      }
      break;
    case 'V'://Set della Vmax
      if (Connect){
        Modo=2;
        Msg("D");
        Cmd = '0';
      }else{
      }
      break;
    case 'N'://Set della Vnow
      if (Connect){
        Modo=3;
        Msg("D");
        Cmd = '0';
      }else{
      }
      break;
    case 'M'://Invio comando di movimento
      if (Connect){
        Modo=4;
        Msg("D");
        Cmd = '0';        
      }else{
        Serial.println("Errore di connessione");
      }
      break;
    case 'E':
      if ((Modo == 1)or(Modo == 2)or(Modo == 3)or(Modo == 4)){
        Modo=0;
        Msg("E");
        //Serial.println("E");
        Cmd = '0';
      }
      break;
    case 'C':
      Connect = HIGH;
      Msg("R");
      digitalWrite(Led, HIGH);
      Cmd='0';
      break;
    default:
      //Serial.println(Modo);
      Val = Cmd;
      if (Modo == 1){
        Serial.print("modo 1: ");
        Vmin = Val;
        Serial.println(Vmin);
      }else if(Modo == 2){
          Serial.print("modo 2: ");
          Vmax = Val;
          Serial.println(Vmax);
        }else if(Modo == 3){
          Serial.print("modo 3: ");
          Vnow = Val;
          MSx.setSpeed(Vnow);
          MDx.setSpeed(Vnow);
          Serial.println(Vnow);
          }else if(Modo == 4){            
            Serial.print("modo 4: ");
            Serial.println(Val);
            Go(Val);
          }else{
          Serial.println("OUT");
        }
      Cmd = '0';
      break;
  }
}

void Go(byte Dir){
  if (Dir == 43){
    MSx.run(FORWARD);
    MDx.run(FORWARD);
  }else if (Dir == 46){
    MSx.run(RELEASE);
    MDx.run(RELEASE);
    MSx.setSpeed(Vnow);
    MDx.setSpeed(Vnow);
  }else if (Dir == 45){
    MSx.run(BACKWARD);
    MDx.run(BACKWARD);
  }else if (Dir == 62){
    MSx.setSpeed(Vmin);
    MSx.run(FORWARD);
  }else if (Dir == 60){
    MDx.setSpeed(Vmin);
    MDx.run(FORWARD);
  }
}

void serialDato(byte Dato){
  char cDato = Dato;
  Serial.print("Ricezione dato: ");
  Serial.print(Dato);
  Serial.print(" - ");
  Serial.println(cDato);
}

void Msg(String M){
  txtMsg = M + '!';
  lStr = txtMsg.length();
  L = 0;
  
}
void ReceiveCMD(int byteCount){
  while (Wire.available()){
    Cmd=Wire.read();
  }
  //serialDato(Cmd);
}

void SData(){
    Wire.write(txtMsg[L]);
    L = L + 1;
    delay(100);
    //Serial.println(L);
}
