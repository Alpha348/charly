#include <AFMotor.h>
#include <Wire.h>


#define Addr 0x08
#define Led 2
#define Vmin 130
#define Vmax 250

AF_DCMotor MSx(4);
AF_DCMotor MDx(3);

int I2cCMD = 0;
byte VSx = Vmax;
byte VDx = Vmax;
byte VSet = Vmax;


void setup(){
  Wire.begin(Addr);
  Wire.onReceive(ReceiveCMD);
  Wire.onRequest(SendData);
  Serial.begin(9600);
  pinMode(Led, OUTPUT);
  digitalWrite(Led, HIGH);
  MSx.setSpeed(VSx);
  MDx.setSpeed(VDx);
  MSx.run(RELEASE);
  MDx.run(RELEASE);
}
void loop(){
  if (I2cCMD!=0){
    Serial.println(I2cCMD);
    if (I2cCMD==115){
      Ferma(VSx,VDx,VSet);
    }else if(I2cCMD==120){
      Go(0,VSx,VDx,VSet);
    }else if(I2cCMD==119){
      Go(1,VSx,VDx,VSet);
    }else if(I2cCMD == 97){
      Turn(0,VSet);
    }else if(I2cCMD == 100){
      Turn(1,VSet);
    }
    I2cCMD=0;
  }
  delay(100);
}
/*----------------------------------------------------*/
void Ferma(byte Vs, byte Vd,byte VSe){
  /*while((Vs>Vmin)and(Vd>Vmin)){
    Serial.print("Vs: ");
    Serial.print(Vs);
    Serial.print(" Vd: ");
    Serial.println(Vd);
    if (Vs>Vmin){
      Vs = Vs - 1;
    }
    if (Vd>Vmin){
      Vd = Vd - 1;
    }
    MSx.setSpeed(Vs);
    MDx.setSpeed(Vd);
    delay(10);
  }*/
  MSx.run(RELEASE);
  MDx.run(RELEASE);
  /*VSx = VSe;
  VDx = VSe;*/
}

void Go(byte M,byte Vs,byte Vd,byte Vx){
  if (M==0x0){
    MSx.run(FORWARD);
    MDx.run(FORWARD);
  }else{
    MSx.run(BACKWARD);
    MDx.run(BACKWARD);
  }
  while ((Vs != Vx)and(Vd != Vx)){
    if (Vs<Vx){
      Vs = Vs + 1;
    }else if (Vs>Vx){
      Vs = Vs - 1;
    }
    if (Vd<Vx){
      Vd = Vd + 1;
    }else if(Vd>Vx){
      Vd = Vd - 1;
    }
    MSx.setSpeed(Vs);
    MDx.setSpeed(Vd);
    delay(10);    
  }
    Serial.print("VSx: ");
    Serial.print(Vs);
    Serial.print(" VDx: ");
    Serial.println(Vd);
}

void Turn(byte M,byte Vs){
  Ferma(VSx,VDx,Vs);
  MDx.setSpeed(Vmin);
  MSx.setSpeed(Vmin);
  if (M==0){
    MDx.run(RELEASE);
    MSx.run(FORWARD);    
  }else if (M==1){
    MDx.run(FORWARD);
    MSx.run(RELEASE);    
  }
  
  
}

void ReceiveCMD(int byteCount){
  while (Wire.available()){
    I2cCMD=Wire.read();
  }

}
void SendData(){
  delay(100);
}
