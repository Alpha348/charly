from tkinter import *
from PIL import ImageTk, Image
from smbus import SMBus
from time import sleep
import threading
import numpy as np
import cv2 as cv

class MiaApp:
    
    def Quit(self):
        self.CamActive=0
        sleep(0.5)
        self.cap.release()
        cv.destroyAllWindows()
        self.mioGenitore.destroy()
    def Cam(self):
        while self.CamActive:            
            ret, frame = self.cap.read()
            if self.CamActive:
                image = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
                image = Image.fromarray(image)
                foto = ImageTk.PhotoImage(image)
                self.LblImg.configure(image=foto)
                self.LblImg.image=foto
            if cv.waitKey(1) == ord('q'):
                self.Quit()
    def Start(self):
        self.CamActive = 1
        self.t2=threading.Thread(target=self.Cam)
        self.t2.start()
        
    def InvioCmd(self, Cmd):
        self.Bus.write_byte(self.addr,Cmd)
        
    def Connect(self):
        if self.Cnt == 0:
            self.InvioCmd(67)
            sleep(100/1000)
            if self.RiceData()=="R!":
                self.Cnt = 1
            else:
                self.Cnt = 0
        return self.Cnt
    
    def RiceData(self):
        D = 'a'
        Data=""
        while D != 33:
            #continua a richiedere dati fino al carattere !
            D = self.Bus.read_byte(self.addr)
            Data = Data + chr(D)
            #print(Data)
        return Data
    
    def SetVal(self, C, V):
        self.InvioCmd(C)
        sleep(0.1)
        if self.RiceData() == "D!":
            print("Modalità SetUp")
            self.InvioCmd(V)
            print("invio uscita modalita setup")
            sleep(0.1)
            self.InvioCmd(69)
            print("Richiesta conferma")
            sleep(0.1);
            if self.RiceData() == "E!":
                print("Confermata uscita modalita SetUp")
        
    def SetCmd(self, C, V):
        self.InvioCmd(C)
        sleep(0.1)
        if self.RiceData() == "D!":        
            self.InvioCmd(V)
            sleep(0.1)
            self.InvioCmd(69)
            sleep(0.1);
            if self.RiceData() == "E!":
                print("Confermata")
                
    def callback(self, d, C):
        #Evento richiamato alla pressione del pulsante
        dire = C[d-1]
        print ("Command: ", dire)
        print ("clicked at", d)
        if (d == 1):
            self.SetCmd(77, 43)
        if (d == 4):
            self.SetCmd(77, 45)
        if (d == 2):
            self.SetCmd(77, 62)
        if (d == 3):
            self.SetCmd(77, 60)
    
    def onkeypress1(self,event):
        #Evento richiamato al rilascio del tasto
        print("jo")
        self.SetCmd(77, 46)
        
    def __init__(self,genitore):
        self.cap = cv.VideoCapture(0)
        self.cap.set(3,200)
        self.cap.set(4,200)
        self.CamActive = 0
        self.Bus = SMBus(1)
        self.addr = 0x8
        self.Dato = 's'
        self.Vmin = 130
        self.Vmax = 250
        self.Vnow = 190
        self.Cnt = 0;#Stato connessione con arduino
        Comandi = ["Avanti","Destra","Sinistra","Indietro","Ferma"]            
        self.ImgRg = ImageTk.PhotoImage(Image.open("Img/FDx.jpg"))
        self.ImgLf = ImageTk.PhotoImage(Image.open("Img/FSx.jpg"))
        self.ImgDw = ImageTk.PhotoImage(Image.open("Img/FGx.jpg"))
        self.ImgUp = ImageTk.PhotoImage(Image.open("Img/FUx.jpg"))
        self.ImgSt = ImageTk.PhotoImage(Image.open("Img/Stop.jpg"))
        
        self.mioGenitore = genitore
        self.Box1 = Frame(genitore,background = "blue")
        self.Box1.pack(fill = BOTH, expand = YES)
        
        self.BoxMv = Frame(self.Box1,
                           background="red",
                           height=219,
                           width=217,
                           borderwidth=2,
                           )        
        self.BoxMv.pack(side = LEFT,expand = YES)
        self.BoxCam = Frame(self.Box1,
                            background="white",
                            height=200,
                            width=200,
                            )
        self.BoxCam.pack(side=RIGHT,padx=5,expand = YES)
        
        self.LblUp = Label(self.BoxMv, image=self.ImgUp)
        self.LblUp.bind("<Button-1>", lambda event, a=1, c=Comandi: self.callback(a,c))
        self.LblUp.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblRg = Label(self.BoxMv, image=self.ImgRg)
        self.LblRg.bind("<Button-1>", lambda event, a=2, c=Comandi: self.callback(a,c))
        self.LblRg.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblLf = Label(self.BoxMv, image=self.ImgLf)
        self.LblLf.bind("<Button-1>", lambda event, a=3, c=Comandi: self.callback(a,c))
        self.LblLf.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblDw = Label(self.BoxMv, image=self.ImgDw)
        self.LblDw.bind("<Button-1>", lambda event, a=4, c=Comandi: self.callback(a,c))
        self.LblDw.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblSt = Label(self.BoxMv, image=self.ImgSt)
        self.LblSt.bind("<Button-1>", lambda event, a=5, c=Comandi: self.callback(a,c))
        self.LblSt.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblImg = Label(self.BoxCam)
        self.LblImg.pack(fill = BOTH, expand = YES)
        
        self.BtuStartCam = Button(self.BoxCam,text="Start", command=self.Start)
        self.BtuStartCam.pack(pady=5,side=LEFT)
        
        self.BtuQuit = Button(self.BoxCam, text="QUIT", command=self.Quit)
        self.BtuQuit.pack(side=RIGHT)
        
        self.LblUp.grid(row=0, column=1)
        self.LblLf.grid(row=1, column=0)
        self.LblSt.grid(row=1, column=1)
        self.LblRg.grid(row=1, column=2)
        self.LblDw.grid(row=2, column=1)
        #*******************************************
        if self.Connect():
            print("Arduino connesso")
            self.SetVal(118,self.Vmin)
            sleep(0.1)
            self.SetVal(86,self.Vmax)
            sleep(0.1)
            self.SetVal(78,self.Vnow)
            
        else:
            print("Arduino disconnesso")
#*************************************************        

root = Tk()
root.geometry("500x220")
root.title("Comandi Charly")
MyCharly = MiaApp(root)
root.mainloop()