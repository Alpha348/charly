#include <Wire.h>
const int LedPin = 2;
void setup() {
  Wire.begin(0x8);
  Wire.onReceive(receiveEvent);
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin,LOW);

}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
}
void receiveEvent(int howMany){
  while(Wire.available()){
    char c = Wire.read();
    digitalWrite(LedPin,c);
  }
}
