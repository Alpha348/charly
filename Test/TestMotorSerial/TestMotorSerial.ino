#include <AFMotor.h>
AF_DCMotor MSx(4);
AF_DCMotor MDx(3);

const byte Led = 2;
const byte Vel = 170;
const byte Step = 20;
const String Menu[] = {"0 - visualizza menu",
"8 - Avanti",
"2 - Indietro",
"4 - MSx lento",
"6 - MDx lento",
"5 - Ferma",
". - VMSx & VMDx = 170"
  };
byte VMSx = 170;
byte VMDx = 170;  
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Motor test start..");

  MSx.setSpeed(170);
  MDx.setSpeed(170);

  MSx.run(RELEASE);
  MDx.run(RELEASE);
  StampaMenu();
}
/*----------------------------------------------
 * ----------------FUNZIONI---------------------
 * ---------------------------------------------
*/
void StampaMenu(){
    for (byte i = 0; i < 7; i = i + 1) {
    Serial.println(Menu[i]);
  }
}

/*********************************************/
void Ferma(){
  int V1;
  int V2;
  V1 = VMSx;
  V2 = VMDx;
  while ((V1>109)and(V2>109)){
    if (V1>109){
      V1 = --V1;
      MSx.setSpeed(V1);
   }
    if (V2 > 109){
      V2 = --V2;
      MDx.setSpeed(V2);
   }
 }
   MSx.run(RELEASE);
   MDx.run(RELEASE);
}
/****************************************/
void Go(byte Dir){
  if (Dir == 1){
    MDx.run(FORWARD);
    MSx.run(FORWARD);
    }else if(Dir == 0){
      MDx.run(BACKWARD);
      MSx.run(BACKWARD);
      }
  while ((VMSx != Vel)and(VMDx != Vel)){
    if (VMSx > Vel){
      VMSx = VMSx - 1;
   }else if(VMSx < Vel){
      VMSx = VMSx + 1;
    }
    if (VMDx > Vel){
      VMDx = VMDx - 1;
   }else if(VMDx < Vel){
      VMDx = VMDx + 1;
    }
   MDx.setSpeed(VMDx);
   MSx.setSpeed(VMSx);
 }
}
/**********************************************************/
void SetV(byte M){
  if (M==1){
      VMSx = VMSx - Step;
      MSx.setSpeed(VMSx);
    }else if (M==0){
      VMDx = VMDx - Step;
      MDx.setSpeed(VMDx);
      }
  }
/*
 * 
 */
void loop() {
  char Dato;
  if (Serial.available()>0){
    Dato = Serial.read();
    if (Dato != '\n'){
      switch (Dato){
        case '8':
          Ferma();
          Go(1);
          break;
        case '5':
          Ferma();
          break;
        case '2':
          Ferma();
          Go(0);
        case '4':
          SetV(1);
          break;
        case '6':
          SetV(0);
          break;
        case '.':
          VMSx = Vel;
          VMDx = Vel;
          MDx.setSpeed(VMDx);
          MSx.setSpeed(VMSx);
          break;
        }
      }
    }

}
