from tkinter import *
from PIL import ImageTk, Image
from smbus import SMBus

class MiaApp:

    def callback(self, d, C):
        #self.bus.write_byte(addr,ord(Dato))
        print ("Command: ", C[d-1])
        print ("clicked at", d)
    
    def onkeypress1(self,event):
        print("jo")
        
    def __init__(self,genitore):
        
        self.Bus = SMBus(1)
        addr = 0x8
        self.Dato = 's'
        Comandi = ["Avanti","Destra","Sinistra","Indietro","Ferma"]
            
        self.ImgRg = ImageTk.PhotoImage(Image.open("Img/FDx.jpg"))
        self.ImgLf = ImageTk.PhotoImage(Image.open("Img/FSx.jpg"))
        self.ImgDw = ImageTk.PhotoImage(Image.open("Img/FGx.jpg"))
        self.ImgUp = ImageTk.PhotoImage(Image.open("Img/FUx.jpg"))
        self.ImgSt = ImageTk.PhotoImage(Image.open("Img/Stop.jpg"))
        
        self.mioGenitore = genitore
        self.Box1 = Frame(genitore,background = "blue")
        self.Box1.pack(fill = BOTH, expand = YES)
        
        self.BoxMv = Frame(self.Box1,
                           background="red",
                           height=219,
                           width=217,
                           borderwidth=2,
                           )        
        self.BoxMv.pack(side = LEFT)
        
        self.LblUp = Label(self.BoxMv, image=self.ImgUp)
        self.LblUp.bind("<Button-1>", lambda event, a=1, c=Comandi: self.callback(a,c))
        self.LblUp.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblRg = Label(self.BoxMv, image=self.ImgRg)
        self.LblRg.bind("<Button-1>", lambda event, a=2, c=Comandi: self.callback(a,c))
        self.LblRg.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblLf = Label(self.BoxMv, image=self.ImgLf)
        self.LblLf.bind("<Button-1>", lambda event, a=3, c=Comandi: self.callback(a,c))
        self.LblLf.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblDw = Label(self.BoxMv, image=self.ImgDw)
        self.LblDw.bind("<Button-1>", lambda event, a=4, c=Comandi: self.callback(a,c))
        self.LblDw.bind("<ButtonRelease-1>", self.onkeypress1)
        
        self.LblSt = Label(self.BoxMv, image=self.ImgSt)
        self.LblSt.bind("<Button-1>", lambda event, a=5, c=Comandi: self.callback(a,c))
        self.LblSt.bind("<ButtonRelease-1>", self.onkeypress1)
        
        
        self.LblUp.grid(row=0, column=1)
        self.LblLf.grid(row=1, column=0)
        self.LblSt.grid(row=1, column=1)
        self.LblRg.grid(row=1, column=2)
        self.LblDw.grid(row=2, column=1)
        
        
root = Tk()
root.geometry("300x220")
root.title("Comandi Charly")
MyCharly = MiaApp(root)
root.mainloop()