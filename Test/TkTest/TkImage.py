from tkinter import *
from PIL import ImageTk, Image

root = Tk()
root.title("Image.py")


my_img1 = ImageTk.PhotoImage(Image.open("Img/FDx.jpg"))
my_img2 = ImageTk.PhotoImage(Image.open("Img/FSx.jpg"))
my_img3 = ImageTk.PhotoImage(Image.open("Img/FGx.jpg"))
my_img4 = ImageTk.PhotoImage(Image.open("Img/FUx.jpg"))
my_img5 = ImageTk.PhotoImage(Image.open("Img/Stop.jpg"))

Image_list = [my_img1,my_img2,my_img3,my_img4,my_img5]

Lbl = Label(image=my_img1)
Lbl.grid(row=0, column=0, columnspan=3)

def forward(image_number):
    global Lbl
    global BtuBack
    global BtuForward
    
    Lbl.grid_forget()
    Lbl = Label(image=Image_list[image_number -1])
    BtuForward = Button(root, text=">>",command=lambda: forward(image_number + 1))
    BtuBack = Button(root, text="<<",command=lambda: Back(image_number - 1))
    if image_number==5:
        BtuForward = Button(root, text=">>",state=DISABLED)
    Lbl.grid(row=0, column=0, columnspan=3)
    BtuForward.grid(row=1, column=2)
    BtuBack.grid(row=1, column=0)
    
                
def Back(image_number):
    global Lbl
    global BtuBack
    global BtuForward
    
    Lbl.grid_forget()
    Lbl = Label(image=Image_list[image_number -1])
    BtuForward = Button(root, text=">>",command=lambda: forward(image_number + 1))
    BtuBack = Button(root, text="<<",command=lambda: Back(image_number - 1))
    if image_number==1:
        BtuBack = Button(root, text="<<", state=DISABLED)
    BtuBack.grid(row=1, column=0)
    BtuForward.grid(row=1, column=2)
    Lbl.grid(row=0, column=0, columnspan=3)

BtuBack = Button(root, text="<<",command=Back, state=DISABLED)
BtuExit = Button(root, text="Exit Program", command=root.quit)
BtuForward = Button(root, text=">>",command=lambda: forward(2))

BtuBack.grid(row=1, column=0)
BtuExit.grid(row=1, column=1)
BtuForward.grid(row=1, column=2)

root.mainloop()