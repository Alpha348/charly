from tkinter import *

def button_click(number):
    current= e.get()
    e.delete(0,END)
    e.insert(0,str(current) + str(number))
    
def button_clear():
    e.delete(0,END)
    
def button_add():
    first_number = e.get()
    global f_num
    global math
    math = "addition"
    f_num = int(first_number)
    e.delete(0, END)
    
def button_subtract():
    first_number = e.get()
    global f_num
    global math
    math = "subtraction"
    f_num = int(first_number)
    e.delete(0, END)

def button_moltiply():
    first_number = e.get()
    global f_num
    global math
    math = "moltiplication"
    f_num = int(first_number)
    e.delete(0, END)

def button_divide():
    first_number = e.get()
    global f_num
    global math
    math = "division"
    f_num = int(first_number)
    e.delete(0, END)


def button_equal():
    second_number = e.get()
    e.delete(0, END)
    if math=="addition":
        e.insert(0, f_num + int(second_number))
    if math=="subtraction":
        e.insert(0, f_num - int(second_number))
    if math=="moltiplication":
        e.insert(0, f_num * int(second_number))
    if math=="division":
        e.insert(0, f_num / int(second_number))
            
    
root = Tk()
root.title("Simple calculator")


e = Entry(root,width=35,borderwidth=5)
e.grid(row=0, column=0, columnspan=3,padx=10,pady=10)

Btu1=Button(root, text="1",padx=40, pady=20, command=lambda: button_click(1))
Btu2=Button(root, text="2",padx=40, pady=20, command=lambda: button_click(2))
Btu3=Button(root, text="3",padx=40, pady=20, command=lambda: button_click(3))
Btu4=Button(root, text="4",padx=40, pady=20, command=lambda: button_click(4))
Btu5=Button(root, text="5",padx=40, pady=20, command=lambda: button_click(5))
Btu6=Button(root, text="6",padx=40, pady=20, command=lambda: button_click(6))
Btu7=Button(root, text="7",padx=40, pady=20, command=lambda: button_click(7))
Btu8=Button(root, text="8",padx=40, pady=20, command=lambda: button_click(8))
Btu9=Button(root, text="9",padx=40, pady=20, command=lambda: button_click(9))
Btu0=Button(root, text="0",padx=40, pady=20, command=lambda: button_click(0))
BtuEqual=Button(root, text="=",padx=91, pady=20, command=button_equal)
BtuClear=Button(root, text="Clear",padx=79, pady=20, command=button_clear)

BtuAdd=Button(root, text="+",padx=39, pady=20, command=button_add)
BtuSub=Button(root, text="-",padx=41, pady=20, command=button_subtract)
BtuMolt=Button(root, text="*",padx=40, pady=20, command=button_moltiply)
BtuDiv=Button(root, text="/",padx=41, pady=20, command=button_divide)


Btu1.grid(row=3, column=0)
Btu2.grid(row=3, column=1)
Btu3.grid(row=3, column=2)

Btu4.grid(row=2, column=0)
Btu5.grid(row=2, column=1)
Btu6.grid(row=2, column=2)

Btu7.grid(row=1, column=0)
Btu8.grid(row=1, column=1)
Btu9.grid(row=1, column=2)

Btu0.grid(row=4, column=0)

BtuClear.grid(row=4, column=1,columnspan=2)
BtuAdd.grid(row=5, column=0)
BtuEqual.grid(row=5, column=1, columnspan=2)

BtuSub.grid(row=6, column=0)
BtuMolt.grid(row=6, column=1)
BtuDiv.grid(row=6, column=2)


root.mainloop()