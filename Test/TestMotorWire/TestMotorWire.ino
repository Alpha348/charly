/*Programma test unzionamento adafruit motor shield
comandato tramite I2c

*/
#include <AFMotor.h>
#include <Wire.h>


#define Addr 0x08
#define Led 2

AF_DCMotor MSx(3);
AF_DCMotor MDx(4);

int I2cCMD = 0;
byte CMD[]={0x0,0x0};
byte VEL[]={0x0,0x0};

void setup(){
  Wire.begin(Addr);
  Wire.onReceive(ReceiveCMD);
  Wire.onRequest(SendData);
  Serial.begin(9600);
  pinMode(Led, OUTPUT);
  digitalWrite(Led, HIGH);
  MSx.setSpeed(200);
  MDx.setSpeed(200);
  MSx.run(RELEASE);
  MDx.run(RELEASE);
}

void loop(){
  if ((CMD[0]!=0x0)and(CMD[1]!=0x0)){
    Serial.println("Comando ricevuto");
    delay(200);
    Serial.println("Interpretazione avviata");
    Interpreta(CMD[1],CMD[0]);
  CMD[0]=0x0;
  CMD[1]=0x0;
  }
}
/*------------------------------------------------*/
void Interpreta(byte Motori, byte Comando){
  if (Comando>0X6D){
    Serial.println("Settaggio velocita");
    setVelocita(Motori,Comando);
  }else if(Comando==0x1){
    Serial.println("Motori avanti");
    ferma(0x3);
    setVelocita(0x1,VEL[0]);
    setVelocita(0x2,VEL[1]);
    MSx.run(FORWARD);
    MDx.run(FORWARD);    
  }else if(Comando==0x2){
    Serial.println("Motori indietro");
    ferma(0x3);
    MSx.run(BACKWARD);
    MDx.run(BACKWARD);
    setVelocita(0x1,VEL[0]);
    setVelocita(0x2,VEL[1]);
  }else if(Comando==0x3){
    ferma(0x3);
    MSx.run(RELEASE);
    MDx.run(RELEASE);
  }else if(Comando==0x4){
    ferma(0x3);
    Serial.println("solo MSx avanti");
    MSx.run(FORWARD);
    setVelocita(0x1,VEL[0]);
    MDx.run(RELEASE);
  }else if(Comando==0x5){
    ferma(0x3);
    MSx.run(RELEASE);
    MDx.run(FORWARD);
    setVelocita(0x2,VEL[1]);
    Serial.println("Solo MDx avanti");
  }

}
/*------------------------------------------------*/
void ferma(byte M){
  byte V1 = VEL[0];
  byte V2 = VEL[1];
  if (M==0x1){
    while (V1>109){
      MSx.setSpeed(V1);
      V1 -= 1;
    }
  }else if(M==0x2){
    while (V2>109){
      MSx.setSpeed(V1);
      V2 -= 1;
    }
  
  }else if(M==0x3){
    Serial.println("Motori fermi");
    while ((V1>80)and(V2>80)){
      MSx.setSpeed(V1);
      MDx.setSpeed(V2);
      V1 -= 1;
      V2 -= 1;
    }
  }
}
/*------------------------------------------------*/
void setVelocita(byte M, byte V){
  Serial.print("M: ");
  Serial.println(M);
  Serial.print("V: ");
  Serial.println(V);
  /*
    M=1 -> MSx
    M=2 -> MDx
    M=3 -> MSx + MDx
  */
  
  if (M==0x01){
    MSx.setSpeed(V);
    VEL[0]=V; 
  }else if(M==0x02){
    MDx.setSpeed(V);
    VEL[1]=V;
  }else if(M==0x03){
    MSx.setSpeed(V);
    MDx.setSpeed(V);
    VEL[0]=V;
    VEL[1]=V;
  }
}


/*------------------------------------------------*/
void ReceiveCMD(int byteCount){
  while (Wire.available()){
    I2cCMD = Wire.read();
  }
  if (CMD[0]==0x0){
    CMD[0]=I2cCMD;
    Serial.print("Cmando :");
    Serial.print(CMD[0]);
  }else if (CMD[1]==0x0){
    CMD[1]=I2cCMD;
    Serial.print(" Valore: ");
    Serial.println(CMD[1]);
  }
//  Serial.println(CMD[0]);
}

void SendData(){
  delay(100);
}
