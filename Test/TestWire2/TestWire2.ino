#include <Wire.h>

const int LedPin = 2;
char Cmd = 0;
byte sData = LOW;
String txtMsg = "";
unsigned int lStr = txtMsg.length();
byte L = 0;

void setup(){
  Wire.begin(0x8);
  Wire.onReceive(RdataEvent);
  Wire.onRequest(SdataEvent);
  Serial.begin(9600);
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, LOW);
  //preparazione del messaggio da inviare
  txtMsg = "Ciao mondo!";
  lStr = txtMsg.length();
}

void loop(){
  delay(100);
  digitalWrite(LedPin, Cmd);
  if (L == lStr){
    L=0;
  }
}

void RdataEvent(int Nbyte){
  while(Wire.available()){
    Cmd = Wire.read();
  }
  Serial.print("Ricevuto: ");
  Serial.println(Cmd);
  Serial.println(char(lStr));
}

void SdataEvent(){
    Wire.write(txtMsg[L]);
    L = L + 1;
    delay(100);
    //Serial.println(L);
}
