from smbus import SMBus
from time import sleep
addr = 0x8
bus = SMBus(1)

Comandi = ['Avanti',"Indietro","Ferma","Avanti Sx","Avanti Dx","Set Velocità"]
Motori = ["Sx","Dx","Sx+Dx"]
#***********************************************#
def RicComando():
	print("Scegli il comando")
	while True:
		for Ic in range(1,7,1):
			print(Ic,  ": ", Comandi[Ic-1])
		Cmd = input(">>>  ")
		if Cmd!="":
			break
	if Cmd=="6":
		print ("Inserire il valore di velocita compresa tra 110 e 255")
		while True:
			Vel = input(">>> ")
			if ((int(Vel)<256)and(int(Vel)>109)):
				Cmd = Vel
				break
	while True:
		print("Seleziona Motore")
		for Im in range(1,4,1):
			print(Im,": ",Motori[Im-1])
		Mx = input(">>> ")
		if Mx!="":
			break
	return (Cmd),(Mx)
#***********************************************#

def sendData(A,B):
	print("Invio comando: ",A)
	bus.write_byte(addr,A)
	sleep(0.2)
	print("Invio Motore: ", B)
	bus.write_byte(addr,B)
	print("Invio terminato")

#***********************************************#
while True:
	Comando,motore  = RicComando()
	Coma = int(Comando)
	if Coma==0:
		break
	Moto = int(motore)
	sendData(Coma,Moto)
