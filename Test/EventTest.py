from tkinter import *

root = Tk()

def key(event):
    print ("pressed", repr(event.char))

def callback(event):
    frame.focus_set()
    print ("clicked at", event.x, event.y)
    
def onkeypress1(event):
        print("jo")
        
frame = Frame(root, width=100, height=100)
frame.bind("<Key>", key)
frame.bind("<Button-1>", callback)
frame.bind("<ButtonRelease-1>", onkeypress1)
frame.pack()

root.mainloop()