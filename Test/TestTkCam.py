from tkinter import *
from PIL import ImageTk, Image
import cv2 as cv

class TestImg:
    def Premi(self):
        ret, img = self.cap.read()
        image = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        foto = ImageTk.PhotoImage(image)
        self.panelA = Label(self.mioContenitore, image = foto)
        self.panelA.image = image
        self.panelA.grid(row=0,column=0)
        
        print("Premuto")
        
    def __init__(self,genitore):
        self.mioGenitore = genitore
        self.mioContenitore= Frame(genitore)
        self.mioContenitore.pack()
        self.cap = cv.VideoCapture(0)
        self.cap.set(3,200)
        self.cap.set(4,200)
        if not self.cap.isOpened():
            print("Cannot open camera")
            exit()
        ret, img = self.cap.read()
        image = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        image = ImageTk.PhotoImage(image)
        self.panelA = Label(self.mioContenitore, image = image)
        self.panelA.image = image
        self.panelA.grid(row=0,column=0)
        self.Btu = Button(self.mioContenitore, text="premi", command=self.Premi)
        self.Btu.grid(row=1,column=0)

root = Tk()
root.geometry("400x200")
root.title("Test")
App = TestImg(root)
root.mainloop()